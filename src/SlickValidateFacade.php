<?php

namespace Itul\SlickValidate;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\SlickValidate\Skeleton\SkeletonClass
 */
class SlickValidateFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'slick-validate';
    }
}
