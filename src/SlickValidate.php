<?php

namespace Itul\SlickValidate;

class SlickValidate
{
	private $_data = [];
	private $_rules = [];
	private $_messages = [];
	public $_validator;

	public function __construct($data = []){
		$this->_data = $data;
		$this->_build();
	}

	public static function make($data = []){
		return (new \Itul\SlickValidate\SlickValidate($data));
	}

	public static function auto($data = []){
		return (new \Itul\SlickValidate\SlickValidate($data))->validate();
	}

	private function _build(){
		$keys = array_keys($this->_data);
		$rules = [];
		foreach($keys as $key) foreach($this->_data[$key] as $ruleData => $ruleMessage){
			if($ruleData == 'credit_card'){

				//CREIT CARD VALIDATION REGEX
				$pattern = "/^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/";
				$ruleData = 'regex:'.$pattern;
			}

			if($ruleData == 'credit_card_cvv') $ruleData = 'digits_between:3,4';

			$this->_rules[$key][] = $ruleData;
			$ruleName = array_filter(explode(':', $ruleData));
			$this->_messages[$key.'.'.array_shift($ruleName)] = $ruleMessage;
		}

		$this->_validator = \Validator::make(request()->all(), $this->_rules, $this->_messages);

		return $this;
	}

	public function fails($callback){
		if($this->_validator->fails()) return $callback($this->_validator);
	}

	public function validate(){
		return $this->_validator->validate();
	}
}
