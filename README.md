# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/slick-validate.svg?style=flat-square)](https://packagist.org/packages/itul/slick-validate)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/slick-validate.svg?style=flat-square)](https://packagist.org/packages/itul/slick-validate)
![GitHub Actions](https://github.com/itul/slick-validate/actions/workflows/main.yml/badge.svg)

This package is meant to automate Laravel validation while customizing Larravel validation messages.

## Installation

You can install the package via composer:

```bash
composer require itul/slick-validate
```


### DOCUMENTATION IS AVAILABLE AT ###
[https://laraveltesting.itulbuild.com/documentation/slick-validate](https://laraveltesting.itulbuild.com/documentation/slick-validate)
